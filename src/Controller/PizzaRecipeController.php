<?php

namespace App\Controller;

use App\Entity\PizzaRecipe;
use App\Form\PizzaRecipeType;
use App\Repository\PizzaRecipeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/pizza-recipe")
 */
class PizzaRecipeController extends AbstractController
{
    /**
     * @Route("/", name="pizza_recipe_index", methods={"GET"})
     */
    public function index(PizzaRecipeRepository $pizzaRecipeRepository): Response
    {
        return $this->render('pizza_recipe/index.html.twig', [
            'pizza_recipes' => $pizzaRecipeRepository->findAll(),
        ]);
    }

    /**
     * @Route("/majeur/{name}", name="pizza_recipe_majeur", methods={"GET"})
     */
    public function majeur(string $name, PizzaRecipeRepository $pizzaRecipeRepository): Response
    {
        return $this->render('pizza_recipe/index.html.twig', [
//            'pizza_recipes' => $pizzaRecipeRepository->findBy(['name' => $name]),
            'pizza_recipes' => $pizzaRecipeRepository->findByName($name),
        ]);
    }

    /**
     * @Route("/annulaire/{name}", name="pizza_recipe_annulaire", methods={"GET"})
     */
    public function annulaire(string $name, PizzaRecipeRepository $pizzaRecipeRepository): Response
    {
        return new Response($pizzaRecipeRepository->count(['name' => $name]));
    }

    /**
     * @Route("/new", name="pizza_recipe_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $pizzaRecipe = new PizzaRecipe();
        $form = $this->createForm(PizzaRecipeType::class, $pizzaRecipe);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($pizzaRecipe);
            $entityManager->flush();

            return $this->redirectToRoute('pizza_recipe_show', ['id' => $pizzaRecipe->getId()]);
        }

        return $this->render('pizza_recipe/new.html.twig', [
            'pizza_recipe' => $pizzaRecipe,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="pizza_recipe_show", methods={"GET"})
     */
    public function show(PizzaRecipe $pizzaRecipe): Response
    {
        return $this->render('pizza_recipe/show.html.twig', [
            'pizza_recipe' => $pizzaRecipe,
        ]);
    }

    /**
     * @Route("/froid/{id}", name="pizza_recipe_froid", methods={"GET"})
     */
    public function froid(int $id): Response
    {
        $pizzaRecipeRepository = $this->getDoctrine()->getRepository(PizzaRecipe::class);
        if (null === $pizzaRecipe = $pizzaRecipeRepository->find($id)){
            return $this->redirectToRoute('pizza_recipe_index');
        }

        return $this->render('pizza_recipe/show.html.twig', [
            'pizza_recipe' => $pizzaRecipe,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="pizza_recipe_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, PizzaRecipe $pizzaRecipe): Response
    {
        $form = $this->createForm(PizzaRecipeType::class, $pizzaRecipe);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('pizza_recipe_index');
        }

        return $this->render('pizza_recipe/edit.html.twig', [
            'pizza_recipe' => $pizzaRecipe,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="pizza_recipe_delete", methods={"DELETE"})
     */
    public function delete(Request $request, PizzaRecipe $pizzaRecipe): Response
    {
        if ($this->isCsrfTokenValid('delete'.$pizzaRecipe->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($pizzaRecipe);
            $entityManager->flush();
        }

        return $this->redirectToRoute('pizza_recipe_index');
    }
}
