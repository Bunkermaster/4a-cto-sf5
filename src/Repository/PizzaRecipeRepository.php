<?php

namespace App\Repository;

use App\Entity\PizzaRecipe;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method PizzaRecipe|null find($id, $lockMode = null, $lockVersion = null)
 * @method PizzaRecipe|null findOneBy(array $criteria, array $orderBy = null)
 * @method PizzaRecipe[]    findAll()
 * @method PizzaRecipe[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PizzaRecipeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PizzaRecipe::class);
    }

    // /**
    //  * @return PizzaRecipe[] Returns an array of PizzaRecipe objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PizzaRecipe
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
